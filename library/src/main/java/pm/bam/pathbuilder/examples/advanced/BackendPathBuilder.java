package pm.bam.pathbuilder.examples.advanced;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import pm.bam.pathbuilder.PathBuilder;
import pm.bam.pathbuilder.examples.simple.CatalogPathBuilder;

public class BackendPathBuilder extends PathBuilder<BackendPathBuilder>
{
    private final String TAG = CatalogPathBuilder.class.getSimpleName();


    // The reference holding the sessionID value
    private String sessionID_value;

    /**
     * Standard constructor that uses an already specified base path, with protocols and ports.
     */
    public BackendPathBuilder()
    {
        super("http", "amazon.com", 80, "/v7/api/");
    }

    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public BackendPathBuilder( String protocol, String baseUrl, String basePath )
    {
        super(protocol, baseUrl, basePath);
    }

    /**
     * Custom constructor where you specific all the variables need for the specific Path you
     * wish to create.
     *
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param port     The port to be used.
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public BackendPathBuilder( String protocol, String baseUrl, int port, String basePath )
    {
        super(protocol, baseUrl, port, basePath);
    }


    /********************
     * CLASS PARAMETERS
     ********************/

    /**
     * This function adds the given SessionID as an additional parameter for the building of the Uri.
     */
    public BackendPathBuilder addSessionID( String sessionID )
    {
        if( !TextUtils.isEmpty(sessionID) )
        { Log.w(TAG, "Given session ID was empty! Warning!"); }
        else
        {
            this.sessionID_value = sessionID;
        }

        return this;
    }

    /********************
     * CLASS OVERRIDES
     ********************/


    /**
     * This function will build the Uri with the provided parameters.
     * <p>
     * First the base Uri will be built with the parameters provided by the constructor.
     * These parameters are mandatory and none of them can be null or empty.
     * <p>
     * Then the additional parameters can be added.
     *
     * @return A Uri with at least a base Uri and possible additional parameters.
     */
    @Override
    public Uri build()
    {
        if( !TextUtils.isEmpty(sessionID_value) )
        { Log.e(TAG, "Given session ID was empty! Error!"); }


        // Gets the base Uri string based on given or stock variables.
        Uri.Builder builder = baseUri();

        /**
         * At this point we have the Base Path, in this case "http://amazon.com:80/v7/api".
         * We must now add the different segments and the desired SessionID.
         * The sessionID can be added before, after or in the middle of the other segments.
         */


        // Add Segment One (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentOne) )
        { builder.appendEncodedPath(segmentOne); }

        // Path is now "http://amazon.com:80/v7/api/[segmentOne]"


        // Add Segment Two (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentTwo) )
        { builder.appendEncodedPath(segmentTwo); }

        // Path is now "http://amazon.com:80/v7/api/[segmentOne]/[segmentTwo]"


        // Add SessionID (function performs NO decoding)
        if( !TextUtils.isEmpty(sessionID_value) )
        { builder.appendEncodedPath(sessionID_value); }

        // Path is now "http://amazon.com:80/v7/api/[segmentOne]/[segmentTwo]/[sessionID]"

        // Add Segment Three (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentThree) )
        { builder.appendEncodedPath(segmentThree); }

        // Path is now 'http://amazon.com:80/v7/api/[segmentOne]/[segmentTwo]/[sessionID]/[segmentThree]'


        // Iterates through all the additional parameters and add at the end of the Path
        if( additionalParameters != null )
        {
            for( NamedParameters namedParameter : additionalParameters )
            {
                builder.appendQueryParameter(namedParameter.name, namedParameter.value);
            }
        }


        return builder.build();
    }
}
