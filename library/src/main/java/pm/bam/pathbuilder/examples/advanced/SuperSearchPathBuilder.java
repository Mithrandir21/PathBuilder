package pm.bam.pathbuilder.examples.advanced;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import pm.bam.pathbuilder.PathBuilder;

/**
 * Created by bam on 20/07/16.
 */
public class SuperSearchPathBuilder extends PathBuilder<SuperSearchPathBuilder>
{
    private final String TAG = SuperSearchPathBuilder.class.getSimpleName();


    // Example of Search API parameter keys
    private final String LIMIT_KEY  = "limit";
    private final String OFFSET_KEY = "offset";

    // The reference holding the sessionID value
    private String sessionID_value;

    /**
     * Standard constructor that uses an already specified base path, with protocols and ports.
     */
    public SuperSearchPathBuilder()
    {
        super("https", "elastic.com", 8888, "/v11/search/");
    }

    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public SuperSearchPathBuilder( String protocol, String baseUrl, String basePath )
    {
        super(protocol, baseUrl, basePath);
    }

    /**
     * Custom constructor where you specific all the variables need for the specific Path you
     * wish to create.
     *
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param port     The port to be used.
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public SuperSearchPathBuilder( String protocol, String baseUrl, int port, String basePath )
    {
        super(protocol, baseUrl, port, basePath);
    }


    /********************
     * CLASS PARAMETERS
     ********************/

    /**
     * This function adds the given Limit as an additional parameter for the building of the Uri.
     */
    public SuperSearchPathBuilder addLimit( int limit )
    {
        if( limit <= 0 )
        { Log.w(TAG, "Given limit was invalid. Skipping."); }
        else
        {
            this.addAdditionalParameters(LIMIT_KEY, limit);
        }

        return this;
    }

    /**
     * This function adds the given Offset as an additional parameter for the building of the Uri.
     */
    public SuperSearchPathBuilder addOffset( int offset )
    {
        if( offset <= 0 )
        { Log.w(TAG, "Given offset was invalid. Skipping."); }
        else
        {
            this.addAdditionalParameters(OFFSET_KEY, offset);
        }

        return this;
    }

    /**
     * This function adds the given SessionID as an additional parameter for the building of the Uri.
     */
    public SuperSearchPathBuilder addSessionID( String sessionID )
    {
        if( !TextUtils.isEmpty(sessionID) )
        { Log.w(TAG, "Given session ID was empty! Warning!"); }
        else
        {
            this.sessionID_value = sessionID;
        }

        return this;
    }

    /********************
     * CLASS OVERRIDES
     ********************/


    /**
     * This function will build the Uri with the provided parameters.
     * <p>
     * First the base Uri will be built with the parameters provided by the constructor.
     * These parameters are mandatory and none of them can be null or empty.
     * <p>
     * Then the additional parameters can be added.
     *
     * @return A Uri with at least a base Uri and possible additional parameters.
     */
    @Override
    public Uri build()
    {
        if( !TextUtils.isEmpty(sessionID_value) )
        { Log.e(TAG, "Given session ID was empty! Error!"); }


        // Gets the base Uri string based on given or stock variables.
        Uri.Builder builder = baseUri();

        /**
         * At this point we have the Base Path, in this case "http://amazon.com:80/v7/api".
         * We must now add the different segments and the desired SessionID.
         * The sessionID can be added before, after or in the middle of the other segments.
         */


        // Add Segment One (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentOne) )
        { builder.appendEncodedPath(segmentOne); }

        // Path is now "https://elastic.com:8888//v11/search/[segmentOne]"


        // Add Segment Two (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentTwo) )
        { builder.appendEncodedPath(segmentTwo); }

        // Path is now "https://elastic.com:8888//v11/search/[segmentOne]/[segmentTwo]"


        // Add SessionID (function performs NO decoding)
        if( !TextUtils.isEmpty(sessionID_value) )
        { builder.appendEncodedPath(sessionID_value); }

        // Path is now "https://elastic.com:8888//v11/search/[segmentOne]/[segmentTwo]/[sessionID]"

        // Add Segment Three (function performs NO decoding)
        if( !TextUtils.isEmpty(segmentThree) )
        { builder.appendEncodedPath(segmentThree); }

        // Path is now 'https://elastic.com:8888//v11/search/[segmentOne]/[segmentTwo]/[sessionID]/[segmentThree]'


        // Iterates through all the additional parameters and add at the end of the Path
        if( additionalParameters != null )
        {
            for( NamedParameters namedParameter : additionalParameters )
            {
                builder.appendQueryParameter(namedParameter.name, namedParameter.value);
            }
        }


        /**
         * At this point, if 'limit' and 'offset' have been added, the url would look like this:
         * 'https://elastic.com:8888//v11/search/[segmentOne]/[segmentTwo]/[sessionID]/[segmentThree]?limit=[int]&offset=[int]'
         */


        return builder.build();
    }
}
