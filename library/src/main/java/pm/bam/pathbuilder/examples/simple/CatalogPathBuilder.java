package pm.bam.pathbuilder.examples.simple;

import android.util.Log;

import pm.bam.pathbuilder.PathBuilder;

public class CatalogPathBuilder extends PathBuilder<CatalogPathBuilder>
{
    private final String TAG = CatalogPathBuilder.class.getSimpleName();


    private final String LIMIT_KEY    = "limit";
    private final String OFFSET_KEY   = "offset";


    /**
     * Standard constructor that uses an already specified base path, with protocols and ports.
     */
    public CatalogPathBuilder()
    {
        super("http", "yahoo.com", "/api");
    }

    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public CatalogPathBuilder( String protocol, String baseUrl, String basePath )
    {
        super(protocol, baseUrl, basePath);
    }

    /**
     * Custom constructor where you specific all the variables need for the specific Path you
     * wish to create.
     *
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param port     The port to be used.
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public CatalogPathBuilder( String protocol, String baseUrl, int port, String basePath )
    {
        super(protocol, baseUrl, port, basePath);
    }


    /********************
     * CLASS PARAMETERS
     ********************/

    /**
     * This function adds the given Limit as an additional parameter for the building of the Uri.
     */
    public CatalogPathBuilder addLimit( int limit )
    {
        if( limit <= 0 )
        { Log.w(TAG, "Given limit was invalid. Skipping."); }
        else
        {
            this.addAdditionalParameters(LIMIT_KEY, limit);
        }

        return this;
    }

    /**
     * This function adds the given Offset as an additional parameter for the building of the Uri.
     */
    public CatalogPathBuilder addOffset( int offset )
    {
        if( offset <= 0 )
        { Log.w(TAG, "Given offset was invalid. Skipping."); }
        else
        {
            this.addAdditionalParameters(OFFSET_KEY, offset);
        }

        return this;
    }
}
