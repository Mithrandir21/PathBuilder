package pm.bam.pathbuilder.examples.simple;

import android.util.Log;

import pm.bam.pathbuilder.PathBuilder;

public class SearchPathBuilder extends PathBuilder<SearchPathBuilder>
{
    private final String TAG = SearchPathBuilder.class.getSimpleName();


    // Example of Search API parameter keys
    private final String SEARCH_KEY   = "q";
    private final String PAGE_KEY     = "page";
    private final String PER_PAGE_KEY = "per_page";


    /**
     * Standard constructor that uses an already specified base path, with protocols and ports.
     */
    public SearchPathBuilder()
    {
        super("http", "google.com", "/");
    }

    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public SearchPathBuilder( String protocol, String baseUrl, String basePath )
    {
        super(protocol, baseUrl, basePath);
    }

    /**
     * Custom constructor where you specific all the variables need for the specific Path you
     * wish to create.
     *
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param port     The port to be used.
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public SearchPathBuilder( String protocol, String baseUrl, int port, String basePath )
    {
        super(protocol, baseUrl, port, basePath);
    }


    /********************
     * CLASS PARAMETERS
     ********************/

    /**
     * This function adds the given Query as an additional parameter for the building of the Uri.
     */
    public SearchPathBuilder addQuery( String query )
    {
        if( query == null || query.length() < 1 )
        { Log.w(TAG, "Given query was invalid. Skipping."); }
        else
        {
            this.addAdditionalParameters(SEARCH_KEY, query);
        }

        return this;
    }

    /**
     * This function adds the given Page as an additional parameter for the building of the Uri.
     */
    public SearchPathBuilder addPage( int page )
    {
        if( page < 0 )
        { Log.w(TAG, "Given page was invalid:" + page + ". Skipping."); }
        else
        {
            this.addAdditionalParameters(PAGE_KEY, page);
        }

        return this;
    }

    /**
     * This function adds the given Per Page as an additional parameter for the building of the Uri.
     */
    public SearchPathBuilder addPerPage( int perPage )
    {
        if( perPage < 1 )
        { Log.w(TAG, "Given per page was invalid:" + perPage + ". Skipping."); }
        else
        {
            this.addAdditionalParameters(PER_PAGE_KEY, perPage);
        }

        return this;
    }
}
