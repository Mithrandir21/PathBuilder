package pm.bam.pathbuilder;

import android.net.Uri;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by bam on 24/08/15.
 */
public abstract class PathBuilder<T extends PathBuilder<T>>
{
    protected String protocol;
    protected String baseUrl;
    protected int port = -1;
    protected String basePath;
    protected String segmentOne;
    protected String segmentTwo;
    protected String segmentThree;


    // The ArrayList that will contain all the additional parameters added by the subclasses.
    protected ArrayList<NamedParameters> additionalParameters = new ArrayList<>();


    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public PathBuilder( String protocol, String baseUrl, String basePath )
    {
        this.simpleValidation(protocol, baseUrl, basePath);

        this.protocol = protocol;
        this.baseUrl = baseUrl;
        this.basePath = basePath;
    }


    /**
     * @param protocol The protocol to be used in the API path. (Example: "Http", "Ftp", etc.)
     * @param baseUrl  The base url of the desired API path. (Example: "google.com", "tv.com", etc.)
     * @param port     The port to be used.
     * @param basePath The base path used. (Example: "v1/rest/api", "v3/api", "v4/access", etc.)
     */
    public PathBuilder( String protocol, String baseUrl, int port, String basePath )
    {
        this.simpleValidation(protocol, baseUrl, port, basePath);

        this.protocol = protocol;
        this.baseUrl = baseUrl;
        this.port = port;
        this.basePath = basePath;
    }


    /**
     * This function will attempt to add the given string as the first segment (after base Uri).
     */
    public final T addSegmentFirst( String segmentOne )
    {
        if( TextUtils.isEmpty(segmentOne) )
        { throw new IllegalArgumentException("Given Segment was null! Error!"); }

        this.segmentOne = segmentOne;
        return (T) this;
    }


    /**
     * This function will attempt to add the given string as the second segment (after segment one).
     */
    public final T addSegmentSecond( String segmentTwo )
    {
        if( TextUtils.isEmpty(segmentTwo) )
        { throw new IllegalArgumentException("Given Segment was empty! Error!"); }

        this.segmentTwo = segmentTwo;
        return (T) this;
    }


    /**
     * This function will attempt to add the given string as the third segment (after segment two).
     */
    public final T addSegmentThird( String segmentThree )
    {
        if( TextUtils.isEmpty(segmentThree) )
        { throw new IllegalArgumentException("Given Segment was empty! Error!"); }

        this.segmentThree = segmentThree;
        return (T) this;
    }


    /**
     * This function creates the baseUri using the given constructor parameters. This should be
     * the base of any Uri build and should specially be used in any subclass implementation of the
     * build() function to get the necessary base Uri.
     */
    protected Uri.Builder baseUri()
    {
        if( port != -1 )
        {
            this.simpleValidation(protocol, baseUrl, port, basePath);
            return Uri.parse(protocol + "://" + baseUrl + ":" + port + basePath).buildUpon();
        }
        else
        {
            this.simpleValidation(protocol, baseUrl, basePath);
            return Uri.parse(protocol + "://" + baseUrl + basePath).buildUpon();
        }
    }


    /**
     * This function adds the given name (KEY) and value to the additional parameters to be used
     * when the Uri is being built. They will added to the Uri after the construction of the
     * base Uri.
     *
     * @param name
     * @param value
     */
    protected void addAdditionalParameters( String name, String value )
    {
        if( TextUtils.isEmpty(name) )
        { throw new IllegalArgumentException("Given Parameter name was empty! Error!"); }

        if( TextUtils.isEmpty(value) )
        { throw new IllegalArgumentException("Given Parameter value was empty! Error!"); }


        // Adds the Key and Value additional parameters
        additionalParameters.add(new NamedParameters(name, value));
    }


    /**
     * This function adds the given name (KEY) and value to the additional parameters to be used
     * when the Uri is being built. They will added to the Uri after the construction of the
     * base Uri.
     *
     * @param name
     * @param value
     */
    protected void addAdditionalParameters( String name, int value )
    {
        addAdditionalParameters(name, "" + value);
    }


    /**
     * This function will build the Uri with the provided parameters.
     * <p/>
     * First the base Uri will be built with the parameters provided by the constructor.
     * These parameters are mandatory and none of them can be null or empty.
     * <p/>
     * Then the additional parameters can be added.
     *
     * @return A Uri with at least a base Uri and possible additional parameters.
     */
    public Uri build()
    {
        // Gets the base Uri string based on given or stock variables.
        Uri.Builder builder = baseUri();


        // Route variable
        if( !TextUtils.isEmpty(segmentOne) )
        {
            // Adds given segmentOne variable (function performs NO decoding).
            builder.appendEncodedPath(segmentOne);
        }


        // Possible ID variable
        if( !TextUtils.isEmpty(segmentTwo) )
        {
            // Adds given segmentTwo variable (function performs NO decoding).
            builder.appendEncodedPath(segmentTwo);
        }


        // Rest Action variable
        if( !TextUtils.isEmpty(segmentThree) )
        {
            String actionString = segmentThree;

            // Adds given Action variable (function performs NO decoding).
            builder.appendEncodedPath(actionString);
        }


        // Iterates through all the additional parameters
        if( additionalParameters != null )
        {
            for( NamedParameters param : additionalParameters )
            {
                builder.appendQueryParameter(param.name, param.value);
            }
        }

        return builder.build();
    }


    /**
     * This function will build the Uri with the provided parameters and return
     * the toString of the built Uri.
     * <p/>
     * First the base Uri will be built with the parameters provided by the constructor.
     * These parameters are mandatory and none of them can be null or empty.
     * <p/>
     * Then the additional parameters can be added.
     *
     * @return A String with at least a base Uri and possible additional parameters.
     */
    public String buildUriString()
    {
        return build().toString();
    }


    /**
     * This inner class will represent a String parameters with a given value.
     */
    public class NamedParameters
    {
        public final String name;
        public final String value;

        public NamedParameters( String name, String value )
        {
            this.name = name;
            this.value = value;
        }
    }


    /**
     * Perform a simple validation of the given parameters to make sure they are within reasonable
     * limits. Very simple checks for empty strings and range of port.
     */
    private void simpleValidation( String protocol, String baseUrl, String basePath )
    {
        if( TextUtils.isEmpty(protocol) )
        { throw new IllegalArgumentException("Given Protocol was null! Error!"); }

        if( TextUtils.isEmpty(baseUrl) )
        { throw new IllegalArgumentException("Given Base URL was null! Error!"); }

        if( TextUtils.isEmpty(basePath) )
        { throw new IllegalArgumentException("Given Base Path was null! Error!"); }
    }


    /**
     * Perform a simple validation of the given parameters to make sure they are within reasonable
     * limits. Very simple checks for empty strings and range of port.
     */
    private void simpleValidation( String protocol, String baseUrl, int port, String basePath )
    {
        if( TextUtils.isEmpty(protocol) )
        { throw new IllegalArgumentException("Given Protocol was null! Error!"); }

        if( TextUtils.isEmpty(baseUrl) )
        { throw new IllegalArgumentException("Given Base URL was null! Error!"); }

        if( port < 1 || port > 65535 )
        { throw new IllegalArgumentException("Given Port was invalid (" + port + ")! Error!"); }

        if( TextUtils.isEmpty(basePath) )
        { throw new IllegalArgumentException("Given Base Path was null! Error!"); }
    }
}
